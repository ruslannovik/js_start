//setTimeout - выполняется единожды 
//setInterval - выполняется постояно с задданным интервалом

//setTimeout(sayHello, 2000); // 2000 -> 2sec
const timer = setInterval(sayHello, 1000);

// clearTimeout, clearInterval()
clearInterval(timer);

// асинхроный 
// event loop

function sayHello() {
    console.log('Hello');
}
