/*
switch(item) {
    case 0:
        //
        break;

    default:
        break;
}
*/

//1. создать массив чисел от 1 до 10 в случайном порядке 
// с помощью одного из изученых методов перебора получить
// массив строк с названиями этих чисел
// [1, 3, 4] => ['один', 'три', 'четыри']
//* если для числа нет словесного описания подстаслять 'hello'
// [2, 1000] => ['два', 'hello']

/*
let arr = new Array(20);

for (let i = 0; i < arr.length; i++) {
    arr[i] = getRandom(1, 10);
}

console.log(arr);

arr.forEach((item, index, array) => {
    if (item === 1) {
        array[index] = 'one';
    } else if (item === 2) {
        array[index] = 'two';
    } else if (item === 3) {
        array[index] = 'three';
    } else {
        array[index] = 'hello';
    }
});

console.log(arr);


function getRandom(min, max) {
    let random = Math.random() * (max - min + 1) + min;
    return Math.floor(random);
}
*/
///////////////////////////////////////////////////////////

let arr = [2, 0, 3, 12, 5, -2, 7, 1];

function getNumberName(number) {
    switch(number) {
        case 1:
            return 'Один';

         case 2:
            return 'Два';

        case 3:
            return 'Три';

        case 4:
            return 'Четыри';

        default:
            return 'Hello';
    }
}



;

/*
const numberNames = [];
for (let i = 0; i < arr.length; i++) {
    //const numberName = getNumberName(arr[i]);
    numberNames.push(getNumberName(arr[i]));
    console.log(numberNames);
}
*/

/*
const numberNames = [];
arr.forEach(item => numberNames.push(getNumberName(arr[item])));
console.log(numberNames);
*/

/*
const numberNames = arr.reduce((accum, item) => accum.concat(getNumberName(item)), []); //(accum, item, index, self)
//
console.log(numberNames);
*/

const numberNames = arr.map(item => getNumberName(item));
console.log(numberNames);