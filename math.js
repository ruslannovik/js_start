
let arr = [2, 0, 3, 12, 45, -2, 7, 33];

//Math.max()
//Math.pow()
//...arr -> спред
//.round - округл до целого по прав мат.
//.floor - округл в меньшую сторону 2.9 -> 2
//.ceil - oкругляет до целого в большую сторону 2,1 -> 3
//.rondom -> генерирует псевдо случаеное число 0 до 1


/*
const maxNumber = Math.max(...arr);
console.log(maxNumber);
*/

/*
const test = Math.pow(3, 1/3);
console.log(test);
*/

/*
const test = Math.round(2.65);
console.log(test);
*/

/*
const random = Math.random();
console.log(random);
*/


function getRandom(min, max) {
    let random = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(random);
}

console.log(getRandom(10, 100));
