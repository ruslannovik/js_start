//let arr = [];

/*
let arr = [
    1, 
    2, 
    3, 
    4, 
    'text', 
    [1, 2, 3]
];
*/

let arr = [3, 5, 2, 1, 5, 7, 9, 12];

/*
console.log(arr[1]);

arr[1] = 25;

console.log(arr[1]);

console.log(arr.length); //.length - длина массива(это метод)

for (let i = 0; i < arr.length; i++) {
    console.log(i + ':' + arr[i]);
}
*/

/*
console.log('--------------');
console.log('Четные элементов:');

for (let i = 0; i < arr. length; i += 2) {
    console.log(arr[i]);
}

console.log('--------------');
console.log('Четные значения элементов:');

for (let i = 0; i < arr. length; i++) {
    if (arr[i] % 2 === 0) console.log(arr[i]);
}
*/

/*
arr.forEach(test); //элемент, индекс, массив

function test(_, index, self) {
    console.log(index);
}
*/

/*
console.log('--------------');
console.log('Нечетные значения элементов:');

arr.forEach(test); //элемент, индекс, массив (arr[i], i, [])

function test(item) {
    if (item % 2 !== 0) console.log(item);
}
*/

/*
arr.forEach(item => {
    if (item % 2 !== 0) {
        console.log(item);
    }
});
*/

/*
arr.forEach(item =>  console.log(item));
*/

/*
let sum = 0;
arr.forEach(item =>  sum += item);

console.log(sum);
*/

/*
.push(значение) - добавляет элемета в конуц массива 
                    длина массива увеличивается на 1
*/

/*
console.log(arr.length);

arr.push(100, 20);

arr.push([30, 40])

console.log(arr.length);

console.log(arr);
*/

/*
unshift(значение) - добавл. эл. в начало, длина увелич на 1
*/

/*
console.log(arr.length);

arr.unshift(100, 20);


console.log(arr.length);

console.log(arr);
*/

/*
.pop() - удаляет последний эл. длина ументш на 1(return)
*/

/*
console.log(arr.length);

arr.push(100);
let last = arr.pop();


console.log(arr.length);

console.log(arr);
console.log(last);
*/

/*
.shift() - удаляет первый эл. длина ументш на 1(return)
*/

/*
console.log(arr.length);

arr.unshift(100);
let first = arr.shift();


console.log(arr.length);

console.log(arr);
console.log(first);
*/

//-----------------------------------------------------------------------------------------------------

/*
.splice(a) - удаляет все элементы масива начиная с а+1
.splice(a, b) - уд. с а+1 в количестве b элементов
.splice(a, b, c) - уд. с а+1 в количестве b элементов и заменяет на c
*/

/*
arr.splice(5);
console.log(arr);

arr.splice(3, 1, 100);
console.log(arr);
*/

// --------------------------------------------------------------------------------------------------------

/*
.indexOf(элемент) - нах первое совподение в массиве и возвращает его индекс
*/

/*
let index = arr.indexOf(5);

console.log(index);

arr.splice(arr.indexOf(5), 1);

console.log(arr);

arr.forEach(_ => {
    let index = arr.indexOf(5);
    if (index !== -1) {
        arr.splice(index, 1);
    }
});

console.log(arr);
*/

/*
.filter(функция) - возвращает новый массив по условию, на исходный не влияет
*/


console.log(arr);

/*
let newArr = arr.filter(moreThan5);

console.log(newArr);

function moreThan5(item) {
    return item > 5;
} 
*/
let newArr = arr.filter(item => item > 5);
console.log(newArr);


/*
.length
.forEach() -> for
.filter()
.push()
.unshift()
.pop()
.shift()
.splice()
.indexOf()
*/





