/*
//1.
//вывести на консоль 4 раза слово 'Hello' с переодичностью 1 сек 
//после чего остановить выполнение

let temerId = setInterval(() => console.log('Hello'), 1000);
setTimeout(() => clearInterval(temerId), 4000);
*/

/*
//2.
//на консоль выводить тек время с помощью setInterval

let next = setInterval(() => {
    console.clear();
    console.log(moment().format('HH:mm:ss'));
    //console.log(getCurrentTime());
}, 1000);
 
function getCurrentTime() {
    let date = new Date();
    let hours = addZero(date.getHours());
    let minutes = addZero(date.getMinutes());
    let seconds = addZero(date.getSeconds());

    return `${hours}:${minutes}:${seconds}`;
}

function addZero(value) {
    return value.toString().length === 1 ? `0${value}` : value;
}
*/

