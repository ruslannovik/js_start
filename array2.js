let arr = [2, 0, 3, 12, 45, -2, 7, 33];

//forEach -> не возвращает значение, работает как for
//filter -> возвращает массив, в конце есле есть {} обязательно return boolen
//find -> возвращает один элемент(первое вхождение), callback должен вернуть boolen
//some -> возращает true  если в массиве присутствует хоть одно вхождение (совпадение),
//        false если совп. нет, callback должен вернуть boolen 
//any -> тоже что и some, но в чистом js его нет
//every -> возращает true  если в массиве все элементы соответствуют условию
//         callback должен вернуть boolen
// map -> возвращает новый массив callback должен вернуть что-нибудь
//reduce (reduceRight)-> (accum, item, index, self) -> возвращает содержимое accum
//                         reduceRight -> с права на лево
// .revers() -> переварачивает массив
// .sort -> (cureent, next) влияет на исходный массив, callback должен возвращать -1, 0, 1

//переменая e -> error или event(событие)



/*
//удаление с помощью filter

console.log(arr);

// задача удалить 12
//.splice(index, 1);
//.indexOf(12);

arr = arr.filter(item => item !== 12);
console.log(arr);
*/


/*
//ищем первое число больше 5
const numberFive = arr.find(element => element > 5);
console.log(numberFive);
*/

/*
//ищем число и заменяем его на 0
console.log(arr);

const numberFive = arr.find(element => element > 5);

if (numberFive) {
    const index = arr.indexOf(numberFive);
    arr.splice(index, 1, 0);
}

console.log(arr);
*/


/*
//ищем есть ли 0
const isHaveZero = arr.some(item => item === 0);
console.log(isHaveZero);
*/


/*
//увеличить числа меньше 5 на 10
console.log(arr);



const newArray = arr.map(item => {
    if (item < 5) {
        return item + 100;
    }

    return item;
});

console.log(newArray);
*/

/*
console.log(arr);

const result = arr.reduce((accum, item) => accum += item, 1000);

console.log(result);
*/

/*
console.log(arr);

arr.reverse();

console.log(arr);
*/


/*
//отсортировать по возврастанию
console.log(arr);

arr.sort((current, next) => current - next);

console.log(arr);
*/


/*
const text = ['Hello', 'good bye', 'warning', 'test', 'first', 'apple', 'Apple'];

console.log(text);

function sortString(a, b) {
    const a_1 = a.toLowerCase();
    const b_1 = b.toLowerCase();
    return a_1.localeCompare(b_1); 
}

text.sort(sortString);

console.log(text);
*/


