

/*
 1. записать в переменую ссылку на тег (по id)
 2. обратится в полученной перенной (к содержиомн тега)
   и установить нужное значение

   1- 
   document
    .getElementsByTagName('название_тега') => []
    .getElementsByClassName('название_класса') => []
    .getElementById('название_id') => ссылка на элемент

    .querySelector('.название_класса', '#название_id', 'название тега') => ссылка на элемент
    .querySelectorAll('.название_класса', 'название тега') => []

    2-
    let myTag
        .innerText = 'Hello';
        .innerHTML = '<div>hello</div>';
*/


const timeTag = document.getElementById('time');

setInterval(() => {
    const currentTime = moment().format('HH:mm:ss');
    timeTag.innerText = currentTime;
}, 1000);


/*
const timeTag = document.getElementById('time');

setInterval(() => {
    const currentTime = moment().format('HH:mm:ss');
    timeTag.innerHTML = '<i>' + currentTime + '</i>';
}, 1000);
*/

// 3. для изменения стилей:
// timeTag
//      .style
//          .color = 'red';
//          .marginTop = '100px';

timeTag.style.color = 'red';
timeTag.style.marginTop = '100px';

/*
 4. для управления классами
    <h1 class="my-class"></h1>

 timeTag
    .className = 'название_класса';
    <h1 class="название_класса"></h1>
    .className = 'название_класса1 название_класса2 название_класса3';

    .classList
        .add('название_класса') -> дописывает новый класс к существуещему если такой уже (есть то ничего не произойдет)
        .remov('название_класса') -> если такой класс найден, то удаляет, если нет то ничего не произойдет

*/

//timeTag.className = 'italic';
timeTag.classList.add('italic');


// ---------------------------------------------------------------------------------------------
const list = document.getElementsByClassName('item');

for (let i = 0; i < list.length; i++) {
    if (i % 2 === 0) {
        //list[i].style.backgroundColor = 'grey';
        list[i].classList.add('even');
    } else {
        //list[i].style.color = 'blue';
        list[i].classList.add('odd');
    }
}
console.log(list);


