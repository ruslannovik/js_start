// ---- Functions-------

/*
function name(a, b) {
// именованая функция
}

function () {
// безымянная функция
}

() => {
    //стрелочная функция
}
*/

// ----------------------------------------------------

sayHello('Dmitry', '');

function sayHello(name, last = 'Temp') {
    console.log('Hollo', name, last);
}

// ----------------------------------------------------

let result = getMulti(3, 2, 5);

console.log(result);

function getSumm(a, b) {
    return a + b;
}


function getMulti(a, b, c) {
    let res = a * getSumm(b, c);

    return res;
}




