

// typeof() - возвращает тип переменой

/*
    console.log();
    document.writeln(); //выводит на страницу сайта
*/

// конкатенация - сложение строк
// NaN - not a number (js не смог строку превести к число)

/*
    +a;
    Number(a);
*/

// a.toString(); // приобразует число к строке

// a === b; - сравнивает типы даных (использовать всегда)
// !!a; - переворачивает два раза;
// '', null, 0, undefined --> false 

// debugger; - это breakpoint

//-------------------------------------------------------------------

//str.toLowerCase() - приведение к нижнему регистру
//str.toUpperCase() - приведение к верхнему регистру

//let myTest = 'My long "long" text'; --> кавычки в кавычках
// `` --> наклоные ковычки, для переноса текста, 

/*
    let myTest = 'My long ' + d + ' long text';
    let myTest = 'My long '.concat(d, ' long text');
    let myTest = `My long ${d} long text ${c}`;
*/

// isConfirm --> 

/*
    alert('Hi World');
    let isConfirm = confirm('Are you shure ?'); // возвращает boolen
    let age = prompt('Enter your age: '); // возвращает string

*/

//----------------------------------------------------------------

// NaN == NaN --> не работает
// isNaN(NaN) --> так работает



let age = prompt('Enter your age: ');

if (Number(age) > 5 && Number(age) <= 100) {
    if (Number(age) >= 18) {
        document.writeln('<h1>Welocme!!!</h1>');
    } else {
        document.writeln('<h2>Good bye!</h2>');
    }
} else {
    alert('Enter correct age!');
}


// if (isNaN(Number(age))) {
//     alert('Incorect dade');
// } else {
//     document.writeln('<h2>Good bye!</h2>');
// }

document.writeln('<h2>Good bye!</h2>');
document.writeln('<h2>Good bye!</h2>');